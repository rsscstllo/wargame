//
//  Player.h
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#ifndef PLAYER_H_
#define PLAYER_H_

// Forward declaration here since this file only needs to know the class exists.
class Card;

#include <vector>    // vector
#include <string>    // string
#include <sstream>   // stringstream
#include <iostream>  // cin and cout

class Player {
public:
  // Constructors
  Player();
  Player(std::vector<Card*> players_hand_, std::string name_, int player_num_,
         int total_score_);
  
  // Accessors and Mutators
  std::vector<Card*> players_hand() const;
  std::string players_name() const;
  int players_num() const;
  int total_score() const;
  void set_players_hand(std::vector<Card*> players_hand);
  void set_players_name(std::string players_name);
  void set_players_num(int players_num);
  void set_total_score(int total_score);
  
  // Functions
  int IncrementTotalScore();
  std::string PlayersHandToString();
  Card* RevealTopCard();
  void AddToPlayersHand(std::vector <Card*> cards_won);
  
  // Destructor
  ~Player(){};
  
private:
  // Instance Variables
  std::vector<Card*> players_hand_;
  std::string players_name_;
  int players_num_;
  int total_score_;
};

#endif // PLAYER_H_
