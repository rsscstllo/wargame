//
//  main.cpp
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#include "Card.h"
#include "Dealer.h"
#include "Player.h"

#include <iostream>
using namespace std;

// Forward delcaration of functions
char MenuSelection();

void WelcomeMenu();

void SetUpSinglePlayerGame(Player* player1_obj_sp, Player* player2_obj_sp);

int GetPlayerNumber();

void SetUpMultiplayerGame(Player* player1_obj_mp, Player* player2_obj_mp);

void SetPlayersNames(string &player1_name, string &player2_name);

string EnterPlayerName();

Card* PlayGame(Player* player_obj, string game_mode, string game_type,
               int war_card_down_num, bool &exit);

void GameMenu(string game_mode, string game_type, string player_name,
              int player_num);

void PrintPlayersTopCard(string game_mode, string game_type, string player_name,
                         Card* player_top_card);

void CompareBattleCards(Player* player1_obj, Player* player2_obj,
                        Card* player1_top_card, Card* player2_top_card,
                        bool &exit, string game_mode);

void CompareWarCards(Player* player1_obj, Player* player2_obj, string game_mode,
                     vector<Card*> &cards_won, int nested_war_num, bool &exit);

void PrintWinnings(Player* player1_obj, Player* player2_obj,
                   string game_mode, string game_type, string player_winner);

void SinglePlayerFinalScore(Player* player1_obj, Player* player2_obj,
                            string player1_name, string player2_name,
                            bool &exit, string game_type);

void MultiplayerFinalScore(Player* player1_obj, Player* player2_obj,
                           bool &exit, string game_type);

void CardsLeftForWar(Player* player1_obj, Player* player2_obj,
                     string player1_name, string player2_name,
                     string player_winner);

void PluralCardsLeft(Player* player_obj);

void PrintLeaderBoard(Player* player1_obj_sp, Player* player2_obj_sp,
                      Player* player1_obj_mp, Player* player2_obj_mp);

void GameInstructions();

int main(int argc, const char * argv[]) {
  // Creating pointers (on the stack) that point to "new" object instances on
  // the heap - the "new" keyword dynamcially allocates memory on the heap.
  // Single player (sp) objects
  Dealer* d1 = new Dealer();
  Player* player1_obj_sp = new Player(d1->GetPlayersHand(1), "Player 1", 1, 0);
  Player* player2_obj_sp = new Player(d1->GetPlayersHand(2), "Player 2", 2, 0);
  // Multiplayer (mp) objects
  Dealer* d2 = new Dealer();
  Player* player1_obj_mp = new Player(d2->GetPlayersHand(1), "Player 1", 1, 0);
  Player* player2_obj_mp = new Player(d2->GetPlayersHand(2), "Player 2", 2, 0);
  while (true) {
    WelcomeMenu();
    // bool variable to break out of first while loop and so the "break"
    // keyword can be used in the nested while loop.
    bool quitting = false;
    while (true) {
      char selection = MenuSelection();
      if (selection == 'a' || selection == 'A') {
        SetUpSinglePlayerGame(player1_obj_sp, player2_obj_sp);
        break;
      } else if (selection == 'b' || selection == 'B') {
        SetUpMultiplayerGame(player1_obj_mp, player2_obj_mp);
        break;
      } else if (selection == 'c' || selection == 'C') {
        PrintLeaderBoard(player1_obj_sp, player2_obj_sp,
                         player1_obj_mp, player2_obj_mp);
        break;
      } else if (selection == 'd' || selection == 'D') {
        GameInstructions();
        cout << endl;
        break;
      } else if (selection == 'e' || selection == 'E') {
        cout << "\nQuitting...\n\n";
        quitting = true;
        break;
      } else {
        cout << "\nInvalid entry. Please try again: ";
      }
    }
    if (quitting) break;
  }
  // Deleting dynamcially allocated memory (on the heap)
  delete d1;
  delete player1_obj_sp;
  delete player2_obj_sp;
  delete d2;
  delete player1_obj_mp;
  delete player2_obj_mp;
  return 0;
}

char MenuSelection() {
  string input = "";
  char menu_selection = {0};
  while (true) {
    getline(cin, input);
    if (input.length() == 1) {
      menu_selection = input[0];
      break;
    }
    cout << "\nInvalid entry. Please try again: ";
  }
  return menu_selection;
}

void WelcomeMenu() {
  cout << "==================================================================\n"
       << "||                        Welcome to War                        ||\n"
       << "==================================================================\n"
       << "||MAIN MENU                                                     ||\n"
       << "||(a) Single player                                             ||\n"
       << "||(b) Multiplayer                                               ||\n"
       << "||(c) View this session's leaderboard                           ||\n"
       << "||(d) How to play war                                           ||\n"
       << "||(e) Quit program                                              ||\n"
       << "==================================================================\n"
       << "Enter choice here: ";
}

void SetUpSinglePlayerGame(Player* player1_obj_sp, Player* player2_obj_sp) {
  string player1_name;
  string player2_name;
  cout << "\nWould you like to be Player 1 or Player 2?\n";
  cout << "Enter either 1 or 2 here: ";
  while (true) {
    int player_num = GetPlayerNumber();
    if (player_num == 1) {
      player1_name = "You";
      player2_name = "Computer";
      cout << "\nPlayer 1 - You\nPlayer 2 - Computer\n\nStarting Game...\n\n";
      break;
    } else if (player_num == 2) {
      player1_name = "Computer";
      player2_name = "You";
      cout << "\nPlayer 1 - Computer\nPlayer 2 - You\n\nStarting Game...\n\n";
      break;
    } else {
      cout << "\nInvalid entry. Please try again: ";
    }
  }
  // Creating new Dealer so that a brand new deck is created every game
  Dealer* new_dealer_obj = new Dealer();
  player1_obj_sp->set_players_hand(new_dealer_obj->GetPlayersHand(1));
  player1_obj_sp->set_players_name(player1_name);
  player2_obj_sp->set_players_hand(new_dealer_obj->GetPlayersHand(2));
  player2_obj_sp->set_players_name(player2_name);
  // bool variable that will be passed by refernce into functions that get user
  // input so this loop can exited if the user decides to quit current game.
  bool exit = false;
  while (player1_obj_sp->players_hand().size() > 0 &&
         player2_obj_sp->players_hand().size() > 0) {
    Card* player1_top_card = PlayGame(player1_obj_sp, "Single Player", "Battle",
                                      0, exit);
    if (exit) break;
    Card* player2_top_card = PlayGame(player2_obj_sp, "Single Player", "Battle",
                                      0, exit);
    if (exit) break;
    CompareBattleCards(player1_obj_sp, player2_obj_sp, player1_top_card,
                       player2_top_card, exit, "Single Player");
    if (exit) break;
  }
  if (player1_obj_sp->players_hand().size() == 0 ||
      player2_obj_sp->players_hand().size() == 0) {
    SinglePlayerFinalScore(player1_obj_sp, player2_obj_sp,
                           player1_obj_sp->players_name(),
                           player2_obj_sp->players_name(), exit, "Battle");
  }
  // Deleting dynamcially allocated memory on the heap.
  delete new_dealer_obj;
}

int GetPlayerNumber() {
  string num_input;
  int correct_int_input;
  char is_there_char;
  while (true) {
    getline(cin, num_input);
    istringstream num_check(num_input);
    // Check to see if an int is in the stringstream without any other chars.
    if ((!(num_check >> correct_int_input)) || (num_check >> is_there_char) ||
        (correct_int_input <= 0))
      cout << "\nInvalid entry. Please try again: ";
    else
      break;
  }
  return correct_int_input;
}

void SetUpMultiplayerGame(Player* player1_obj_mp, Player* player2_obj_mp) {
  string player1_name;
  string player2_name;
  SetPlayersNames(player1_name, player2_name);
  // Creating new Dealer so that a brand new deck is created every game.
  Dealer* new_dealer_obj = new Dealer();
  player1_obj_mp->set_players_hand(new_dealer_obj->GetPlayersHand(1));
  player1_obj_mp->set_players_name(player1_name);
  player2_obj_mp->set_players_hand(new_dealer_obj->GetPlayersHand(2));
  player2_obj_mp->set_players_name(player2_name);
  // bool variable that will be passed by refernce into functions that get user
  // input so this loop can exited if the user decides to quit current game.
  bool exit = false;
  while (player1_obj_mp->players_hand().size() > 0 &&
         player2_obj_mp->players_hand().size() > 0) {
    Card* player1_top_card = PlayGame(player1_obj_mp, "Multiplayer", "Battle",
                                      0, exit);
    if (exit) break;
    Card* player2_top_card = PlayGame(player2_obj_mp, "Multiplayer", "Battle",
                                      0, exit);
    if (exit) break;
    CompareBattleCards(player1_obj_mp, player2_obj_mp, player1_top_card,
                       player2_top_card, exit, "Multiplayer");
    if (exit) break;
  }
  if (player1_obj_mp->players_hand().size() == 0 ||
      player2_obj_mp->players_hand().size() == 0) {
    MultiplayerFinalScore(player1_obj_mp, player2_obj_mp, exit, "Battle");
  }
  // Deleting dynamcially allocated memory on the heap
  delete new_dealer_obj;
}

void SetPlayersNames(string &player1_name, string &player2_name) {
  cout << "\nPlayer 1 - ";
  player1_name = EnterPlayerName();
  cout << "Player 2 - ";
  player2_name = EnterPlayerName();
  cout << "\nStarting Game...\n\n";
}

string EnterPlayerName() {
  string player_name = "";
  cout << "Enter your name: ";
  getline(cin, player_name);
  return player_name;
}

// This function is in charge of returning a Card object pointer when each
// player reveals his or her card as well as prompting the user with a menu
// when necessary.
Card* PlayGame(Player* player_obj, string game_mode, string game_type,
               int war_card_down_num, bool &exit) {
  // Creating null card object just to satisfy this function's return type.
  Card* null_card = NULL;
  // This if statement is for the first three face-down cards a during war.
  if (war_card_down_num >= 1 && war_card_down_num < 4) {
    Card* war_card_down = player_obj->RevealTopCard();
    return war_card_down;
    // This if statement is only for the Computer's card.
  } else if (game_mode == "Single Player" &&
             player_obj->players_name() == "Computer") {
    Card* computer_top_card = player_obj->RevealTopCard();
    GameMenu("Single Player", game_type, player_obj->players_name(),
             player_obj->players_num());
    PrintPlayersTopCard(game_mode, game_type, player_obj->players_name(),
                        computer_top_card);
    return computer_top_card;
  }
  // This will always the prompt the user for input during a single player game
  // or both players during a multiplayer game.
  while (true) {
    // Instead of using "game_mode" as the parameter for GameMenu(), use
    // "Multiplayer" since this while loop will only execute when it's a user's
    // turn and not the Computer's turn during a single player game and thus
    // should print the same kind of menu to the user regardless of game mode.
    GameMenu("Multiplayer", game_type, player_obj->players_name(),
             player_obj->players_num());
    char selection;
    while (true) {
      selection = MenuSelection();
      if (selection == 'a' || selection == 'A') {
        if (game_type == "Battle") {
          Card* top_card = player_obj->RevealTopCard();
          PrintPlayersTopCard(game_mode, "Battle", player_obj->players_name(),
                              top_card);
          return top_card;
        } else if (game_type == "War") {
          Card* fourth_card = player_obj->RevealTopCard();
          cout << "\nCard 1: face-down\nCard 2: face-down"
               << "\nCard 3: face-down\nCard 4: ";
          PrintPlayersTopCard(game_mode, "War", player_obj->players_name(),
                              fourth_card);
          return fourth_card;
        }
      } else if (selection == 'b' || selection == 'B') {
        cout << endl;
        exit = true;
        break;
      } else {
        cout << "\nInvalid entry. Please try again: ";
      }
    }
    if (exit) break;
  }
  return null_card;
}

void GameMenu(string game_mode, string game_type, string player_name,
              int player_num) {
  if (game_mode == "Multiplayer") {
    if (game_type == "Battle") {
      cout << "Player " << player_num  << "'s turn"
           << " - " << player_name << ":\n"
           << "(a) Reveal your top card.\n"
           << "(b) End current game and return to main menu.\n"
           << "Enter choice here: ";
    } else if (game_type == "War") {
      cout << "Player " << player_num << "'s turn"
           << " - " << player_name << ":\n"
           << "(a) Draw three cards face-down and a fourth face-up.\n"
           << "(b) End current game and return to main menu.\n"
           << "Enter choice here: ";
    }
  } else if (game_mode == "Single Player") {
    if (game_type == "Battle") {
      cout << "Player " << player_num  << "'s turn"
           << " - " << player_name << ":\n";
    } else if (game_type == "War") {
      cout << "Player " << player_num  << "'s turn"
           << " - " << player_name << ":\n\n";
      cout << "Card 1: face-down\nCard 2: face-down"
           << "\nCard 3: face-down\nCard 4: ";
    }
  }
}

void PrintPlayersTopCard(string game_mode, string game_type, string player_name,
                         Card* player_top_card) {
  if (game_mode == "Multiplayer") {
    if (game_type == "Battle") {
      cout << endl << player_name << "'s card is: "
           << player_top_card->CardToString() << endl << endl;
    } else if (game_type == "War") {
      cout << player_top_card->CardToString() << endl << endl;
    }
  } else if (game_mode == "Single Player") {
    if (game_type == "Battle" && player_name == "You") {
      cout << endl << "Your card is: "
           << player_top_card->CardToString() << endl << endl;
    } else if (game_type == "Battle" && player_name == "Computer") {
      cout << "Computer's card is: "
           << player_top_card->CardToString() << endl << endl;
    } else if (game_type == "War") {
      cout << player_top_card->CardToString() << endl << endl;
    }
  }
}

// This function takes in both player objects as well as each of their card
// objects and compares the rank to determine the winner for each battle. If
// the card ranks are the same, the CompareWarCards() function is called.
void CompareBattleCards(Player* player1_obj, Player* player2_obj,
                        Card* player1_top_card, Card* player2_top_card,
                        bool &exit, string game_mode) {
  // Vector of Card objects that stores the cards drawn from each player.
  vector<Card*> cards_won;
  // Add both player's drawn cards to the vector cards_won.
  cards_won.push_back(player1_top_card);
  cards_won.push_back(player2_top_card);
  // Check winner and add the cards to appropriate player's hand.
  if (player1_top_card->card_rank() > player2_top_card->card_rank()) {
    player1_obj->AddToPlayersHand(cards_won);
    PrintWinnings(player1_obj, player2_obj, game_mode, "Battle", "Player 1");
  } else if (player1_top_card->card_rank() < player2_top_card->card_rank()) {
    player2_obj->AddToPlayersHand(cards_won);
    PrintWinnings(player1_obj, player2_obj, game_mode, "Battle", "Player 2");
  } else if (player1_top_card->card_rank() == player2_top_card->card_rank()) {
    CompareWarCards(player1_obj, player2_obj, game_mode, cards_won, 1, exit);
  }
}

// Recursive algorithm for generating and comparing the subsequent cards drawn
// after a battle for a war. This function takes in both player objects as well
// as the vector that has their previously drawn battle cards.
void CompareWarCards(Player* player1_obj, Player* player2_obj, string game_mode,
                     vector<Card*> &cards_won, int nested_war_num, bool &exit) {
  // Inform the user when a war occurs and the number of a nested War.
  if (nested_war_num == 1) cout << "!!WAR!!\n\n";
  else if (nested_war_num == 2) cout << "!!DOUBLE WAR!!\n\n";
  else if (nested_war_num == 3) cout << "!!TRIPLE WAR!!\n\n";
  else cout << "!!WAR X" << nested_war_num << "!!\n\n";
  // Check to make sure each player has enough cards to continue with the war.
  if (player1_obj->players_hand().size() < 4 ||
      player2_obj->players_hand().size() < 4) {
    if (game_mode == "Single Player") {
      SinglePlayerFinalScore(player1_obj, player2_obj,
                             player1_obj->players_name(),
                             player2_obj->players_name(), exit, "War");
    } else if (game_mode == "Multiplayer") {
      MultiplayerFinalScore(player1_obj, player2_obj, exit, "War");
    }
  }
  // Create each player's four cards and check to see if they quit after the
  // fourth card since that's the one that prints out a menu when called
  while (player1_obj->players_hand().size() >= 4 &&
         player2_obj->players_hand().size() >= 4) {
    Card* player1_down1 = PlayGame(player1_obj, game_mode, "War", 1, exit);
    Card* player1_down2 = PlayGame(player1_obj, game_mode, "War", 2, exit);
    Card* player1_down3 = PlayGame(player1_obj, game_mode, "War", 3, exit);
    Card* player1_face4 = PlayGame(player1_obj, game_mode, "War", 4, exit);
    if (exit) break;
    Card* player2_down1 = PlayGame(player2_obj, game_mode, "War", 1, exit);
    Card* player2_down2 = PlayGame(player2_obj, game_mode, "War", 2, exit);
    Card* player2_down3 = PlayGame(player2_obj, game_mode, "War", 3, exit);
    Card* player2_face4 = PlayGame(player2_obj, game_mode, "War", 4, exit);
    if (exit) break;
    // Add both player's drawn cards to the vector cards_won.
    cards_won.push_back(player1_down1);
    cards_won.push_back(player1_down2);
    cards_won.push_back(player1_down3);
    cards_won.push_back(player1_face4);
    cards_won.push_back(player2_down1);
    cards_won.push_back(player2_down2);
    cards_won.push_back(player2_down3);
    cards_won.push_back(player2_face4);
    // Check winner and add the cards to appropriate player's hand.
    if (player1_face4->card_rank() > player2_face4->card_rank()) {
      player1_obj->AddToPlayersHand(cards_won);
      PrintWinnings(player1_obj, player2_obj, game_mode, "War", "Player 1");
      break;
    } else if (player1_face4->card_rank() < player2_face4->card_rank()) {
      player2_obj->AddToPlayersHand(cards_won);
      PrintWinnings(player1_obj, player2_obj, game_mode, "War", "Player 2");
      break;
    } else if (player1_face4->card_rank() == player2_face4->card_rank()) {
      // Recursive step - increment nested_war_num
      CompareWarCards(player1_obj, player2_obj, game_mode, cards_won,
                      ++nested_war_num, exit);
      break;
    }
    if (exit) break;
  }
}

// Print winnings for Battles and Wars and for Single Player and Multiplayer.
void PrintWinnings(Player* player1_obj, Player* player2_obj,
                   string game_mode, string game_type, string player_winner) {
  if (game_mode == "Multiplayer") {
    if (player_winner == "Player 1") {
      cout << ">> " << player1_obj->players_name();
      cout << " wins this " << game_type << "!\n";
    } else if (player_winner == "Player 2") {
      cout << ">> " << player2_obj->players_name();
      cout << " wins this " << game_type << "!\n";
    }
    cout << ">> " << player1_obj->players_name() << " now has ";
    cout << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << ">> " << player2_obj->players_name() << " now has ";
    cout << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << endl;
  } else if (game_mode == "Single Player" &&
             player1_obj->players_name() == "Computer") {
    if (player_winner == "Player 1") {
      cout << ">> " << player1_obj->players_name();
      cout << " wins this " << game_type << "!\n";
    } else if (player_winner == "Player 2") {
      cout << ">> " << player2_obj->players_name();
      cout << " win this " << game_type << "!\n";
    }
    cout << ">> " << player1_obj->players_name() << " now has ";
    cout << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << ">> " << player2_obj->players_name() << " now have ";
    cout << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << endl;
  } else if (game_mode == "Single Player" &&
             player2_obj->players_name() == "Computer") {
    if (player_winner == "Player 1") {
      cout << ">> " << player1_obj->players_name();
      cout << " win this " << game_type << "!\n";
    } else if (player_winner == "Player 2") {
      cout << ">> " << player2_obj->players_name();
      cout << " wins this " << game_type << "!\n";
    }
    cout << ">> " << player1_obj->players_name() << " now have ";
    cout << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << ">> " << player2_obj->players_name() << " now has ";
    cout << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << endl;
  }
}

// This function is only as long as it is to compensate for proper verb usage
// since "You" and "Computer" are different points of view (2nd and 3rd person)
// and thus require different forms of the verbs (is/are, have/has, win/wins).
void SinglePlayerFinalScore(Player* player1_obj, Player* player2_obj,
                            string player1_name, string player2_name,
                            bool &exit, string game_type) {
  if (game_type == "Battle" && player1_name == "Computer") {
    // There will never be a case when both players have a hand < 1.
    if (player1_obj->players_hand().size() < 1) {
      player2_obj->IncrementTotalScore();
      cout << player1_name << " is out of cards...\n"
           << player2_name << " WIN THE GAME!\n\n";
    } else if (player2_obj->players_hand().size() < 1) {
      player1_obj->IncrementTotalScore();
      cout << player2_name << " are out of cards...\n"
           << player1_name << " WINS THE GAME!\n\n";
    }
  } else if (game_type == "Battle" && player2_name == "Computer") {
    if (player1_obj->players_hand().size() < 1) {
      player2_obj->IncrementTotalScore();
      cout << player1_name << " are out of cards...\n"
           << player2_name << " WINS THE GAME!\n\n";
    } else if (player2_obj->players_hand().size() < 1) {
      player1_obj->IncrementTotalScore();
      cout << player2_name << " is out of cards...\n"
           << player1_name << " WIN THE GAME!\n\n";
    }
  } else if (game_type == "War" && player1_name == "Computer") {
    if (player2_obj->players_hand().size() < 4 &&
        player1_obj->players_hand().size() >= 4) {
      player1_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "Computer");
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() >= 4) {
      player2_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "You");
      // If enough nested wars occur (WAR X7 with an unshuffled deck).
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() < 4) {
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "Neither_Single_Player");
      if (player1_obj->players_hand().size() >
          player2_obj->players_hand().size()) {
        player1_obj->IncrementTotalScore();
        cout << player1_name << " WINS THIS GAME!\n\n";
      } else if (player1_obj->players_hand().size() <
                 player2_obj->players_hand().size()) {
        player2_obj->IncrementTotalScore();
        cout << player2_name << " WIN THIS GAME!\n\n";
      } else if (player1_obj->players_hand().size() ==
                 player2_obj->players_hand().size()) {
        cout << "\nYou and the computer have the same amount of cards left,"
             << " so no\none wins this game. \n\n";
      }
    }
  } else if (game_type == "War" && player2_name == "Computer") {
    if (player2_obj->players_hand().size() < 4 &&
        player1_obj->players_hand().size() >= 4) {
      player1_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "You");
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() >= 4) {
      player2_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "Computer");
      // If enough nested wars occur (Testing: WAR X7 with an unshuffled deck).
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() < 4) {
      CardsLeftForWar(player1_obj, player2_obj, player1_name, player2_name,
                      "Neither_Single_Player");
      if (player1_obj->players_hand().size() >
          player2_obj->players_hand().size()) {
        player1_obj->IncrementTotalScore();
        cout << player1_name << " WIN THIS GAME!\n\n";
      } else if (player1_obj->players_hand().size() <
                 player2_obj->players_hand().size()) {
        player2_obj->IncrementTotalScore();
        cout << player2_name << " WINS THIS GAME!\n\n";
      } else if (player1_obj->players_hand().size() ==
                 player2_obj->players_hand().size()) {
        cout << "\nYou and the computer have the same amount of cards left,"
             << " so no\none wins this game. \n\n";
      }
    }
  }
  cout << "Score\n" << "Player 1: " << player1_obj->total_score() << endl
       << "Player 2: " << player2_obj->total_score() << endl << endl;
  exit = true;
}

void MultiplayerFinalScore(Player* player1_obj, Player* player2_obj,
                           bool &exit, string game_type) {
  if (game_type == "Battle") {
    if (player1_obj->players_hand().size() < 1) {
      player2_obj->IncrementTotalScore();
      cout << player1_obj->players_name() << " is out of cards...\n"
           << player2_obj->players_name() << " WINS THE GAME!\n\n";
    } else if (player2_obj->players_hand().size() < 1) {
      player1_obj->IncrementTotalScore();
      cout << player2_obj->players_name() << " is out of cards...\n"
           << player1_obj->players_name() << " WINS THE GAME!\n\n";
    }
  } else if (game_type == "War") {
    if (player2_obj->players_hand().size() < 4 &&
        player1_obj->players_hand().size() >= 4) {
      player1_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_obj->players_name(),
                      player2_obj->players_name(), "Player 1");
      
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() >= 4) {
      player2_obj->IncrementTotalScore();
      CardsLeftForWar(player1_obj, player2_obj, player1_obj->players_name(),
                      player2_obj->players_name(), "Player 2");
      
    } else if (player1_obj->players_hand().size() < 4 &&
               player2_obj->players_hand().size() < 4) {
      CardsLeftForWar(player1_obj, player2_obj, player1_obj->players_name(),
                      player2_obj->players_name(), "Neither");
      if (player1_obj->players_hand().size() >
          player2_obj->players_hand().size()) {
        player1_obj->IncrementTotalScore();
        cout << player1_obj->players_name() << " WINS THIS GAME!\n\n";
        
      } else if (player1_obj->players_hand().size() <
                 player2_obj->players_hand().size()) {
        player2_obj->IncrementTotalScore();
        cout << player2_obj->players_name() << " WINS THIS GAME!\n\n";
        
      } else if (player1_obj->players_hand().size() ==
                 player2_obj->players_hand().size()) {
        cout << "\nBoth players have the same amount of cards left, so no one"
             << " wins \nthis game. \n\n";
      }
    }
  }
  cout << "Score\n" << "Player 1: " << player1_obj->total_score() << endl
       << "Player 2: " << player2_obj->total_score() << endl << endl;
  exit = true;
}

// Prints out appropriate message if a player does not have enough cards to
// continue with the current war and thus loses the game.
void CardsLeftForWar(Player* player1_obj, Player* player2_obj,
                     string player1_name, string player2_name,
                     string player_winner) {
  if (player_winner == "Player 1") {
    cout << player2_name << " does not have enough cards for this war...\n\n";
    cout << player1_name << " has " << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << player2_name << " has " << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << player1_name << " WINS THIS GAME!\n\n";
    
  } else if (player_winner == "Player 2") {
    cout << player1_name << " does not have enough cards for this war...\n\n";
    cout << player1_name << " has " <<  player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << player2_name << " has " <<  player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << player2_name << " WINS THIS GAME!\n\n";
    
  } else if (player_winner == "Neither") {
    cout << "Neither player has enough cards to continue with this war...\n\n";
    cout << player1_name << " has " << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << player2_name << " has " << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    
  } else if (player_winner == "Computer") {
    cout << player2_name << " do not have enough cards for this war...\n\n";
    cout << player1_name << " has " << player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << player2_name << " have " << player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << player1_name << " WINS THIS GAME!\n\n";
    
  } else if (player_winner == "You") {
    cout << player1_name << " does not have enough cards for this war...\n\n";
    cout << player1_name << " has " <<  player1_obj->players_hand().size();
    PluralCardsLeft(player1_obj);
    cout << player2_name << " have " <<  player2_obj->players_hand().size();
    PluralCardsLeft(player2_obj);
    cout << player2_name << " WIN THIS GAME!\n\n";
    
  } else if (player_winner == "Neither_Single_Player") {
    cout << "Neither player has enough cards to continue with this war...\n\n";
    if (player1_name == "Computer") {
      cout << player1_name << " has " << player1_obj->players_hand().size();
      PluralCardsLeft(player1_obj);
      cout << player2_name << " have " << player2_obj->players_hand().size();
      PluralCardsLeft(player2_obj);
    } else if (player2_name == "Computer") {
      cout << player1_name << " have " << player1_obj->players_hand().size();
      PluralCardsLeft(player1_obj);
      cout << player2_name << " has " << player2_obj->players_hand().size();
      PluralCardsLeft(player2_obj);
    }
  }
}

void PluralCardsLeft(Player* player_obj) {
  if (player_obj->players_hand().size() > 1)
    cout << " cards left.\n";
  else if (player_obj->players_hand().size() == 1)
    cout << " card left.\n";
  else if (player_obj->players_hand().size() == 0)
    cout << " cards left.\n";
}

void GameInstructions() {
  cout << "\nWar is a card game involving two players and a standard deck of\n"
       << "52 cards. Once shuffled, the deck is split evenly between them\n"
       << "so each player has a stack of 26 cards face-down. Each player\n"
       << "then reveals his or her top card face-up for both players to see.\n"
       << "Whoever has the higher ranked card (card suit is trivial),\n"
       << "collects both cards that were placed down. This is known as a \n"
       << "\"Battle.\" If both cards have the same rank or value, then \n"
       << "each player draws three cards from his or her stack and places\n"
       << "them face-down next to the card previously shown. A fourth card is\n"
       << "then subsequently drawn from each player's stack, though this one\n"
       << "is placed face-up next to the three face-down cards. The ranks of\n"
       << "these last two face-up cards are then compared just as before, and\n"
       << "whoever has the higher ranked card, collects all the cards that\n"
       << "were drawn - both face-down cards and face-up cards. This is known\n"
       << "as a \"War\" and can happen as many times as two cards of equal \n"
       << "rank are drawn. This process of Battles, and Wars when they occur,\n"
       << "is repeated until a player runs out of cards. Whoever runs out of \n"
       << "cards first loses the game... Good luck!\n\n"
       << "Enter \'r\' to return back to the main menu: ";
  while (true) {
    char selection = MenuSelection();
    if (selection == 'r' || selection == 'R')
      break;
    else
      cout << "\nInvalid entry. Please try again: ";
  }
}

// Prints out the sores for both players for single player and multiplayer
// games and determines who, out of all four players, has the higest score.
// This only keeps track of the actual player object and not the player object's
// name. So, if a user is player 1 for one session and wins, then plays a new
// game, this time as player 2 and wins, the score for player 1 and 2 will
// both be incremented and thus the same.
// A possible solution to this is to create another function that takes in each
// player object by reference as its parameters. The function could then store
// each player object's name in a vector of strings, which could be searched for
// any matching names (if the same player, for multiplayer) entered his same
// name, but as a different player, 1 or 2) and then combine their scores to
// update their total score. A similar approach could be used for single player,
// keeping track of how many "You"'s and how many "Computer"'s won each game and
// then combining thier scores.
void PrintLeaderBoard(Player* player1_obj_sp, Player* player2_obj_sp,
                      Player* player1_obj_mp, Player* player2_obj_mp) {
  cout << "\nSingle player games\n";
  cout << "Player 1: ";
  cout << player1_obj_sp->total_score() << endl;
  cout << "Player 2: ";
  cout << player2_obj_sp->total_score() << endl;
  
  cout << "\nMultiplayer games\n";
  cout << "Player 1: ";
  cout << player1_obj_mp->total_score() << endl;
  cout << "Player 2: ";
  cout << player2_obj_mp->total_score() << endl;
  
  if (player1_obj_sp->total_score() > player2_obj_sp->total_score() &&
      player1_obj_sp->total_score() > player1_obj_mp->total_score() &&
      player1_obj_sp->total_score() > player2_obj_mp->total_score()) {
    cout << "\nCurrent leader: Single player - Player 1!\n\n";
    
  } else if (player2_obj_sp->total_score() > player1_obj_sp->total_score() &&
             player2_obj_sp->total_score() > player1_obj_mp->total_score() &&
             player2_obj_sp->total_score() > player2_obj_mp->total_score()) {
    cout << "\nCurrent leader: Single player - Player 2!\n\n";
    
  } else if (player1_obj_mp->total_score() > player1_obj_sp->total_score() &&
             player1_obj_mp->total_score() > player2_obj_sp->total_score() &&
             player1_obj_mp->total_score() > player2_obj_mp->total_score()) {
    cout << "\nCurrent leader: Multiplayer - Player 1!\n\n";
    
  } else if (player2_obj_mp->total_score() > player1_obj_sp->total_score() &&
             player2_obj_mp->total_score() > player2_obj_sp->total_score() &&
             player2_obj_mp->total_score() > player1_obj_mp->total_score()) {
    cout << "\nCurrent leader: Multiplayer - Player 2!\n\n";
    
  } else if (player1_obj_sp->total_score() == player2_obj_sp->total_score() ==
             player1_obj_mp->total_score() == player2_obj_mp->total_score()) {
    cout << "\nThere is no current leader yet. Go back to the main menu and\n"
         << "start a new game!\n\n";
  }
  cout << "Enter \'r\' to return back to the main menu: ";
  while (true) {
    char selection = MenuSelection();
    if (selection == 'r' || selection == 'R')
      break;
    else
      cout << "\nInvalid entry. Please try again: ";
  }
  cout << endl;
}
