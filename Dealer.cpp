//
//  Dealer.cpp
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#include "Dealer.h"
#include "Card.h"

// Default Constructor
// Although the deck vector of pointers to card objects (the_deck_) is alredy
// a member of this class, a temporary vector of 52 null pointers is made so
// that when a new game is started (and the dealer class is called) a new deck
// will be created. The original deck vector is swapped with the temporary
// vector and inherits the 52 null pointers which then become redefined via
// the for loop iteration. The card rank is (i % 13) so that the value falls
// within the index values of 0-12 (duece-ace). Example with i = 2, (i % 13).
// 2/13 = 0 with a remainder of 2... Having a smaller number before the modulo
// operator cuases the answer to be itself.
Dealer::Dealer() {
  std::vector<Card*> temp_vec(52, nullptr);
  the_deck_.swap(temp_vec);
  for (unsigned int i = 0; i < 52; i++) {
    if (i < 13)
      the_deck_[i] = new Card((i % 13), "Clubs");
    else if (i < 26)
      the_deck_[i] = new Card((i % 13), "Diamonds");
    else if (i < 39)
      the_deck_[i] = new Card((i % 13), "Hearts");
    else if (i < 52)
      the_deck_[i] = new Card((i % 13), "Spades");
  }
  // Seeds the pseudo random number generator that rand() uses
  srand((unsigned int) time(0));
  // Making a random number from 1 to 100
  int randomNum = (rand() % 100) + 1;
  // For loop to have the deck be shuffled that amount of times
  for (int i = 0; i < randomNum; ++i)
    ShuffleTheDeck();
  // Delete the temporary vector's pointer indices.
  for (unsigned int i = 0; i < temp_vec.size(); i++)
    delete temp_vec[i];
}

// Initialization Constructor
Dealer::Dealer(std::vector<Card*> the_deck_) : the_deck_(the_deck_) {}

std::vector<Card*> Dealer::the_deck() const {
  return the_deck_;
}

void Dealer::set_the_deck(std::vector<Card*> the_deck) {
  the_deck_ = the_deck;
}

void Dealer::ShuffleTheDeck() {
  random_shuffle(the_deck_.begin(), the_deck_.end());
}

// Prints out the full deck of cards and was mainly used only for debugging
// purposes - not used anywhere in the main.
std::string Dealer::TheDeckToString() {
  std::stringstream ss;
  for (unsigned int i = 0; i < the_deck_.size(); i++)
    ss << the_deck_[i]->CardToString() << "\n";
  return ss.str();
}

// Gets each player's hand of 26 cards. Player 1 gets the first 26 cards and
// player 2 gets the second 26 cards of the deck, after it has been shuffled.
// Player 2 does not have to be specified in the if statement becuase the game
// only involves two players.
std::vector<Card*> Dealer::GetPlayersHand(int player_num) {
  std::vector<Card*> players_hand;
  if (player_num == 1) {
    for (unsigned int i = 0; i < 26; i++)
      players_hand.push_back(the_deck_[i]);
  } else {
    for (unsigned int i = 26; i < 52; i++)
      players_hand.push_back(the_deck_[i]);
  }
  return players_hand;
}
