//
//  Player.cpp
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#include "Player.h"
#include "Card.h"

// Default Constructor
Player::Player() {}

// Initialization Constructor
// When this class is called, it will take in a vector named players_hand_
// that points to card objects. This vector will come from the GetPlayersHand
// function in the dealer class. So, when the player class is called in the
// main, it will pass the GetPlayersHand function as its parameter. This
// creates each player-object's hand of 26 cards.
Player::Player(std::vector<Card*> players_hand_, std::string players_name_,
               int players_num_, int total_score_)
    : players_hand_(players_hand_), players_name_(players_name_),
      players_num_(players_num_), total_score_(total_score_) {}

std::vector<Card*> Player::players_hand() const {
  return players_hand_;
}

std::string Player::players_name() const {
  return players_name_;
}

int Player::players_num() const {
  return players_num_;
}

int Player::total_score() const {
  return total_score_;
}

void Player::set_players_hand(std::vector<Card*> players_hand) {
  players_hand_ = players_hand;
}

void Player::set_players_name(std::string players_name) {
  players_name_ = players_name;
}

void Player::set_players_num(int players_num) {
  players_num_ = players_num;
}

void Player::set_total_score(int total_score) {
  total_score_ = total_score;
}

// Adds one to the player's total score after winning a game.
int Player::IncrementTotalScore() {
  total_score_ += 1;
  return total_score_;
}

// Prints out the full hand of 26 cards for each player object - mainly used
// for debugging purposes.
std::string Player::PlayersHandToString() {
  std::stringstream ss;
  for (unsigned int i = 0; i < players_hand_.size(); i++)
    ss << players_hand_[i]->CardToString() << "\n";
  return ss.str();
}

// Makes a new pointer to a card object that is set equal to the player's top
// card (0th index) in his or her hand. The player's top card is now deleted
// from his or her hand since it has been played though returned as a new card
// for comparison against the other player's card.
Card* Player::RevealTopCard() {
  Card* top_card;
  top_card = players_hand_[0];
  players_hand_.erase(players_hand_.begin());
  return top_card;
}

// Adds the cards from the "battle" or "war" to the winning player's hand.
void Player::AddToPlayersHand(std::vector <Card*> cards_won) {
  for (unsigned int i = 0; i < cards_won.size(); i++)
    players_hand_.push_back(cards_won[i]);
}
