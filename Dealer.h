//
//  Dealer.h
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#ifndef DEALER_H_
#define DEALER_H_

// Forward declaration here since this file only needs to know the class exists.
class Card;

#include <ctime>        // time()
#include <string>       // string
#include <vector>       // vector
#include <sstream>      // stringstream
#include <cstdlib>      // srand()
#include <algorithm>    // random_shuffle()

class Dealer {
public:
  // Constructors
  Dealer();
  Dealer(std::vector<Card*> the_deck_);
  
  // Accessors and Mutators
  std::vector<Card*> the_deck() const;
  void set_the_deck(std::vector<Card*> the_deck);
  
  // Functions
  void ShuffleTheDeck();
  std::string TheDeckToString();
  std::vector<Card*> GetPlayersHand(int player_num);
  
  // Destructor
  ~Dealer(){};
  
private:
  // Instance Variables
  std::vector<Card*> the_deck_;
};

#endif // DEALER_H_
