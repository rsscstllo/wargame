OBJS   = main.o Card.o Dealer.o Player.o
CC     = g++
DEBUG  = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAGS = -Wall $(DEBUG)

all: war

war: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o war

main.o: main.cpp Card.h Dealer.h Player.h
	$(CC) $(CFLAGS) main.cpp

Card.o: Card.h Card.cpp
	$(CC) $(CFLAGS) Card.cpp

Dealer.o: Dealer.h Dealer.cpp Card.h
	$(CC) $(CFLAGS) Dealer.cpp

Player.o: Player.h Player.cpp Card.h
	$(CC) $(CFLAGS) Player.cpp

clean:
	rm *.o *.tar war

tar:
	tar cfv WarGame.tar main.cpp Card.h Card.cpp Dealer.h Dealer.cpp \
Player.h Player.cpp Makefile
