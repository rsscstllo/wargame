//
//  Card.cpp
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#include "Card.h"

Card::Card() : card_rank_(0), card_suit_("Clubs") {}

// Initialization Constructor
// When this class is called, it will take in two parameters - its card rank
// and card suit. The respective parameters will be passed in during the
// creation of the deck for each suit causing this class to be called four
// different times for each suit. The card rank is handled in the dealer class.
Card::Card(int card_rank_, std::string card_suit_)
    : card_rank_(card_rank_), card_suit_(card_suit_) {}

// Returns the rank of a card to be compared during the game.
int Card::card_rank() const {
  return card_rank_;
}

std::string Card::card_suit() const {
  return card_suit_;
}

void Card::set_card_rank(int card_rank) {
  card_rank_ = card_rank;
}

void Card::set_card_suit(std::string card_suit) {
  card_suit_ = card_suit;
}

// Converts each numerical card rank to its representable string, which can
// then be printed out along with its suit.
std::string Card::CardToString() {
  std::string card_rank_str = "";
  
  if      (card_rank_ == 0)  card_rank_str = "Deuce";
  else if (card_rank_ == 1)  card_rank_str = "Three";
  else if (card_rank_ == 2)  card_rank_str = "Four";
  else if (card_rank_ == 3)  card_rank_str = "Five";
  else if (card_rank_ == 4)  card_rank_str = "Six";
  else if (card_rank_ == 5)  card_rank_str = "Seven";
  else if (card_rank_ == 6)  card_rank_str = "Eight";
  else if (card_rank_ == 7)  card_rank_str = "Nine";
  else if (card_rank_ == 8)  card_rank_str = "Ten";
  else if (card_rank_ == 9)  card_rank_str = "Jack";
  else if (card_rank_ == 10) card_rank_str = "Queen";
  else if (card_rank_ == 11) card_rank_str = "King";
  else if (card_rank_ == 12) card_rank_str = "Ace";
  
  std::stringstream ss;
  ss << card_rank_str << " of " << card_suit_;
  return ss.str();
}
