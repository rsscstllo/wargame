//
//  Card.h
//  game_cards_war
//
//  Created by Ross Castillo on 5/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#ifndef CARD_H_
#define CARD_H_

#include <string>   // string
#include <sstream>  // stringstream

class Card {
public:
  // Constructors
  Card();
  Card(int card_rank_, std::string card_suit_);
  
  // Accessors and Mutators
  int card_rank() const;
  std::string card_suit() const;
  void set_card_rank(int card_rank);
  void set_card_suit(std::string card_suit);
  
  // Functions
  std::string CardToString();
  
  // Destructor
  ~Card(){};
  
private:
  // Instance Variables
  int card_rank_;
  std::string card_suit_;
};

#endif  // CARD_H_
