# README #

### About This Repository ###

* This project is console application that simulates the card game War.  
* Coded during the last few weeks of my freshman year.  
* v1.0.0  

### How To Set Up ###

* Download the project, open Terminal, and type the following commands once in the home directory of the 
project:  
1. `make`  
2. `./war`  

#  

* Additionaly, after running the program, type:  
1. `make tar` for a zipped copy of the project, and  
2. `make clean` to remove the generated object files as well as the zipped copy.  
  
#### Source Files ####
* main.cpp  
* Card.cpp Card.h  
* Dealer.cpp Dealer.h  
* Player.cpp Player.h  
